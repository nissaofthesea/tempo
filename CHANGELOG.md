# changelog
## [unreleased]

## [0.9.0] - 2025-01-04
### changed
* `tempo` now quits when EOF is received in place of a command
* bumped zig version to 0.13.0

## [0.8.0] - 2024-05-22
### changed
* bumped zig version to 0.12.0
* simplified ringbuf truncation
* added more assertions

## [0.7.4] - 2023-11-17
### fixed
* fix: negative size is invalid and shouldn't be clamped

## [0.7.3] - 2023-11-11
### added
* added error code to fatal error messages

### fixed
* arg allocation failure message not flushed
* typo in README

## [0.7.2] - 2023-11-01
### changed
* simplfied readln
* added more assertions and tests

## [0.7.1] - 2023-10-14
### fixed
* fixed windows build compile error

### changed
* check clock support immediately
  * initial support check added before event loop

## [0.7.0] - 2023-10-13
### changed
* bumped zig version to 0.11.0
* remove nix build files
* codebase is "nicer"
  * more assertions
  * more tests

## [0.6.0] - 2023-08-02
### added
* added goodbye message to quit command
  * this makes it clearer that `tempo` exited successfully

### fixed
* improved accuracy of average bpm computation
  * see [this blog post](https://www.nu42.com/2015/03/how-you-average-numbers.html)

### changed
* rewritten in Zig

## [0.5.4] - 2023-07-31
* last supported Rust version

## [0.5.3] - 2023-07-27
### added
* added manpage for tempo(1)

### fixed
* don't print user input for unrecognized command, may contain escape codes

## [0.5.2] - 2023-07-20
### added
* added `default.nix` and `shell.nix` for building with nix

### fixed
* fixed incorrect capacity when buffer size is greater than `Tapper::MAX_CAP`

### changed
* warn user when buffer size is clamped
* fail if passed any arguments

## [0.5.1] - 2023-07-16
### fixed
* fixed reversed printing order

## [0.5.0] - 2023-07-16
### added
* added print command

### fixed
* ignore empty string for buffer size

### changed
* use stack allocated buffer
* removed caching

## [0.4.1] - 2022-12-18
### fixed
* account for accumulating floating point error in cache

## [0.4.0] - 2022-10-18
### changed
* relicensed under MIT or Apache 2.0
* lowered read limit (no. bytes allowed to read from stdin)
* no longer escapes special characters in input

## [0.3.2] - 2022-09-24
### changed
* cache bpm

## [0.3.1] - 2022-09-18
### added
* added crate documentation
* published on crates.io

### changed
* made commands case-insensitive

## [0.3.0] - 2022-06-29
### added
* added bound command
  * switch between bounded and unbounded buffer

### changed
* changed description to "terminal tempo tapper"

### fixed
* fixed delayed buffer resize

## [0.2.0] - 2022-06-29
### added
* added size command
  * size command resizes the buffer
* **BREAKING:** replaced reset command with clear command
  * clear command clears the buffer
* unrecognized command error includes user input

### changed
* changed algorithm to averaging a buffer of samples
* aesthetically improved interface

## [0.1.0] - 2022-06-29
### added
* added help command
* added tap command
* added reset command
* added license command
* added quit command
