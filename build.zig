const std = @import("std");

const fmt = std.fmt;
const fs = std.fs;
const mem = std.mem;

const meta = @import("src/meta.zig");

pub fn build(b: *std.Build) !void {
    const man_pages = b.option(
        bool,
        "man-pages",
        "Whether to build man pages [default: false]",
    ) orelse false;

    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = meta.BIN_NAME,
        .version = meta.PKG_VERSION,
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
        .single_threaded = true,
        .linkage = .static,
    });
    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const unit_tests = b.addTest(.{
        .root_source_file = b.path("src/tests.zig"),
        .target = target,
        .optimize = optimize,
    });
    const run_unit_tests = b.addRunArtifact(unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);

    if (man_pages) {
        try scdocInstall(b, "docs/tempo.1.scd");
    }
}

// https://github.com/riverwm/river and https://github.com/mitchellh/libxev build.zig were helpful here
fn scdocInstall(
    b: *std.Build,
    path: []const u8, // ex. docs/tempo.1.scd
) !void {
    const scd_out = path[0..mem.lastIndexOfScalar(u8, path, '.').?]; // ex. docs/tempo.1

    // build
    {
        const cmd = try fmt.allocPrint(b.allocator, "scdoc < {s} > {s}", .{ path, scd_out });
        _ = b.run(&[_][]const u8{ "sh", "-c", cmd });
    }

    // install
    {
        const name = fs.path.basename(scd_out); // ex. tempo.1
        const section = fs.path.extension(name)[1..]; // ex. 1
        const output = try fmt.allocPrint(b.allocator, "share/man/man{s}/{s}", .{ section, name });
        b.installFile(scd_out, output);
    }
}
