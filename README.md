# tempo

`tempo` is a terminal tempo tapper.

```
...

1/10 samples in buffer
106.3 bpm
 * <enter>

2/10 samples in buffer
110.4 bpm
 * c

0/10 samples in buffer
0.0 bpm
 ; █
```

## concept

`tempo` works by collecting bpms into a buffer as you tap.

```
[120.5, 124.3, 122.6, _, _, _, _, _, _, _]
 ^                                      ^
new bpms inserted here        old bpms removed here
```

the final bpm is the average of all samples in the buffer.

## usage

to use `tempo`, tap to the desired tempo, and it will display the average bpm.

for more fine tuning, you can also adjust the size of the buffer.
the bpm may be more accurate the more samples are averaged.

| command   | description                     |
|-----------|---------------------------------|
| h         | show help                       |
| \<enter\> | register a tap                  |
| c         | clear buffer                    |
| s         | adjust buffer size              |
| b         | bound or unbound buffer to size |
| p         | print buffer contents           |
| l         | print license info              |
| q         | quit                            |

## installation

to install the program:

```console
$ zig build -Doptimize=ReleaseSafe --prefix "FIXME" install
```

`tempo` also has documentation available as man pages.
to install those alongside the program, pass `-Dman-pages` to your invocation of `zig build`.

## license

`tempo` is licensed under the MIT or Apache 2.0 licenses, at your choice.
