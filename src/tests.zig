// tempo: terminal tempo tapper
// copyright (C) 2022-2023 Nissa <and-nissa@protonmail.com>
// licensed under MIT OR Apache-2.0

const std = @import("std");

test "ringbuf" {
    std.testing.refAllDecls(@import("ringbuf.zig"));
}

test "tap" {
    std.testing.refAllDecls(@import("tap.zig"));
}
