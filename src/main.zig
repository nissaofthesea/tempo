// tempo: terminal tempo tapper
// copyright (C) 2022-2023 Nissa <and-nissa@protonmail.com>
// licensed under MIT OR Apache-2.0

// TODO: we kinda assume ascii or utf8 but we also operate on strings as bytes

const std = @import("std");

const ascii = std.ascii;
const assert = std.debug.assert;
const enums = std.enums;
const fmt = std.fmt;
const fs = std.fs;
const heap = std.heap;
const io = std.io;
const math = std.math;
const mem = std.mem;
const os = std.os;
const process = std.process;

pub const tap = @import("tap.zig");
pub const ringbuf = @import("ringbuf.zig");
pub const meta = @import("meta.zig");
const rb = ringbuf;

const File = fs.File;

const BadUsage = error{BadUsage};
const TempoError = BadUsage || File.ReadError || File.WriteError || tap.Tapper.Error;

const ARGS_ALLOC_BUF_SIZE: usize = 1024;
const INPUT_BUF_SIZE: usize = 128;
const STDOUT_BUF_SIZE: usize = 256;
const STDERR_BUF_SIZE: usize = 64;

const DEFAULT_BUF_CAP: rb.Idx = 10;
const DEFAULT_BOUNDED: bool = true;

pub fn main() void {
    var stdout = bufferedWriterWithSize(STDOUT_BUF_SIZE, io.getStdOut().writer());
    var stderr = bufferedWriterWithSize(STDERR_BUF_SIZE, io.getStdErr().writer());

    var exit_code: u8 = 0;

    tryMain(&stdout, &stderr) catch |err| {
        const try_repr = switch (err) {
            TempoError.BadUsage => null, // tryMain already took care of error message
            TempoError.ClockUnsupported => "a suitable clock was not detected for this platform",
            else => "input/output error",
        };
        if (try_repr) |repr| {
            stderr.writer().print("fatal error: {s} ({})\n", .{ repr, err }) catch {};
        }
        exit_code = 1;
    };

    stdout.flush() catch {};
    stderr.flush() catch {};
    process.exit(exit_code);
}

fn tryMain(stdout: anytype, stderr: anytype) TempoError!void {
    var wout = stdout.writer();

    // ensure we're not being passed any arguments
    try checkUsage(stderr);
    try stderr.flush();

    // check that the clock is supported (at least currently)
    try tap.Tapper.checkSupported();

    var tapper = tap.Tapper.init(DEFAULT_BUF_CAP, DEFAULT_BOUNDED);
    var input = std.BoundedArray(u8, INPUT_BUF_SIZE).init(0) catch unreachable;

    try splashText(wout);

    while (true) {
        try wout.print("\n", .{});

        // print the bpm and buffer stats
        try wout.print("{}/{}{s} samples in buffer\n", .{
            tapper.count(),
            tapper.capacity(),
            if (tapper.isBounded()) "" else "+",
        });
        try tap.formatBpm(wout, tapper.bpm());
        try wout.print(" bpm\n", .{});

        try readln(
            stdout,
            &input,
            if (tapper.isRecording()) " * " else " ; ",
        );
        const try_cmd = switch (input.len) {
            0 => .Quit, // quit on EOF by convention
            else => Command.fromStr(trim(input.slice())),
        };

        if (try_cmd) |cmd| {
            switch (cmd) {
                .Help => {
                    try wout.print("\n", .{});
                    for (enums.values(Command)) |c| {
                        try wout.print(" {s}. {s}.\n", .{ c.shortName(), c.description() });
                    }
                },

                .Tap => try tapper.tap(),

                .Clear => tapper.clear(),

                .Size => {
                    try wout.print("\n", .{});

                    try readln(stdout, &input, " new buffer size? ");
                    const trimmed = trim(input.slice());
                    if (trimmed.len == 0) continue;

                    var try_cap: ?rb.Idx = null;
                    if (fmt.parseUnsigned(rb.Idx, trimmed, 10)) |cap| {
                        try_cap = cap;
                    } else |err| {
                        switch (err) {
                            fmt.ParseIntError.Overflow => try_cap = math.maxInt(rb.Idx),
                            fmt.ParseIntError.InvalidCharacter => {
                                try wout.print(" invalid character in integer\n", .{});
                            },
                        }
                    }
                    if (try_cap) |cap| {
                        tapper.resize(cap);
                        const reported = tapper.capacity();
                        assert(reported <= cap);
                        if (reported < cap) {
                            try wout.print(" size too large, clamped to {}\n", .{reported});
                        }
                    }
                },

                .Bound => tapper.toggleBounded(),

                .Print => {
                    try wout.print("\n", .{});
                    try wout.print(" ", .{});
                    try tap.formatTapper(wout, &tapper);
                    try wout.print("\n", .{});
                },

                .License => {
                    try wout.print("\n", .{});
                    try wout.print(" copyright (C) 2022-2023 {s}\n", .{meta.PKG_AUTHORS});
                    try wout.print(" licensed under {s}\n", .{meta.PKG_LICENSE});
                },

                .Quit => {
                    try wout.print("\n", .{});
                    try wout.print(" goodbye\n", .{});
                    break;
                },
            }
        } else {
            try wout.print("\n", .{});
            try wout.print(" unrecognized command. try \"h\" for help.\n", .{});
        }
    }
}

fn checkUsage(stderr: anytype) (BadUsage || File.WriteError)!void {
    const werr = stderr.writer();
    var args_alloc_buf: [ARGS_ALLOC_BUF_SIZE]u8 = undefined;
    var args_alloc = heap.FixedBufferAllocator.init(&args_alloc_buf);
    var try_args = process.argsWithAllocator(args_alloc.allocator());
    if (try_args) |*args| {
        defer args.deinit();
        const name = args.next();
        if (args.skip()) {
            werr.print("usage: {s}\n", .{name.?}) catch {};
            return TempoError.BadUsage;
        }
    } else |err| {
        // we can't check argc but that's not a fatal error
        try werr.print("error: failed to allocate arguments: {any}\n", .{err});
    }
}

fn splashText(wout: anytype) File.WriteError!void {
    try wout.print("{s} {}: {s}\n", .{
        meta.BIN_NAME,
        meta.PKG_VERSION,
        meta.PKG_DESCRIPTION,
    });
    try wout.print("type \"h\" for help, \"l\" for license\n", .{});
}

fn readln(
    out: anytype,
    input: *std.BoundedArray(u8, INPUT_BUF_SIZE),
    prompt: []const u8,
) (File.WriteError || File.ReadError)!void {
    try out.writer().print("{s}", .{prompt});
    try out.flush();
    var stdin = io.getStdIn().reader();
    const new_len = try stdin.read(&input.buffer);
    input.resize(new_len) catch unreachable;
}

fn trim(s: []const u8) []const u8 {
    return mem.trim(u8, s, &ascii.whitespace);
}

fn bufferedWriterWithSize(
    comptime buffer_size: usize,
    underlying_stream: anytype,
) io.BufferedWriter(buffer_size, @TypeOf(underlying_stream)) {
    return .{ .unbuffered_writer = underlying_stream };
}

const Command = enum {
    Help,
    Tap,
    Clear,
    Size,
    Bound,
    Print,
    License,
    Quit,

    const Self = @This();

    pub fn fromStr(s: []const u8) ?Self {
        for (enums.values(Self)) |cmd| {
            if (mem.eql(u8, s, cmd.literal())) {
                return cmd;
            }
        }
        return null;
    }

    pub fn literal(self: Self) []const u8 {
        return switch (self) {
            .Help => "h",
            .Tap => "",
            .Clear => "c",
            .Size => "s",
            .Bound => "b",
            .Print => "p",
            .License => "l",
            .Quit => "q",
        };
    }

    pub fn shortName(self: Self) []const u8 {
        return switch (self) {
            .Tap => "<enter>",
            else => |other| other.literal(),
        };
    }

    pub fn description(self: Self) []const u8 {
        return switch (self) {
            .Help => "show help",
            .Tap => "register a tap",
            .Clear => "clear buffer",
            .Size => "adjust buffer size",
            .Bound => "bound or unbound buffer to size",
            .Print => "print buffer contents",
            .License => "print license info",
            .Quit => "quit",
        };
    }
};
