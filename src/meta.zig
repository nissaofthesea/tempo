// tempo: terminal tempo tapper
// copyright (C) 2022-2023 Nissa <and-nissa@protonmail.com>
// licensed under MIT OR Apache-2.0

// as zig package management develops this will become more hacky

const std = @import("std");

pub const BIN_NAME = "tempo";
pub const PKG_VERSION: std.SemanticVersion = .{ .major = 0, .minor = 9, .patch = 0 };
pub const ZIG_VERSION: std.SemanticVersion = .{ .major = 0, .minor = 13, .patch = 0 };
pub const PKG_DESCRIPTION = "terminal tempo tapper";
pub const PKG_AUTHORS = "Nissa <and-nissa@protonmail.com>";
pub const PKG_LICENSE = "MIT OR Apache-2.0";
